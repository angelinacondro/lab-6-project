from django.db import models

# Create your models here.
class subscribe_model(models.Model):
	email = models.EmailField(max_length = 30)
	name = models.CharField(max_length = 30)
	password = models.CharField(max_length = 30)

	def as_dict(self):
		return{
			"email" : self.email,
			"name" : self.name
		}
