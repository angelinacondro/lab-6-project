from django.conf.urls import url
from .views import webservice, validate_email, posting, listofsubscriber, listsubjson, unsubscribe

#url for app
urlpatterns = [
	url('webservice', webservice, name='webservice'),
	url('/validate_email/', validate_email, name='validate_email'),
	url('posting/', posting, name = 'posting'),
	url('listofsubscriber/', listofsubscriber, name = 'listofsubs'),
	url('/listsubjson/', listsubjson, name='listsubjson'),
	url('unsubscribe/', unsubscribe, name='unsubscribe'),
	url('', webservice, name='webservice')
]
