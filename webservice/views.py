from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import subscribe_form
from .models import subscribe_model
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse



def webservice(request):
	form = subscribe_form(request.POST or None)
	
	response = {
	 	"form" : subscribe_form
	}
	return render(request, 'webservice.html', response)

def posting(request):
	# print('hhee')
	# html = 'webservice.html'
	# # response = {
	# # 	"form" : subscribe_form,
	# # 	"data" : subscribe_model.objects.all()
	# # }
	# form = subscribe_form
	# data = subscribe_model.objects.all()
	# print('hhee')
	form = subscribe_form(request.POST or None)
	if request.method == 'POST' and form.is_valid:
		# print('haha')
		name = request.POST['name']
		email = request.POST['email']
		password = request.POST['password']
		submodel = subscribe_model(email = email, name = name, password = password)
		submodel.save()
		return JsonResponse({'is_success': True})
		# response['email'] = request.POST['email']
		# response['name'] = request.POST['name']
		# response['password'] = request.POST['password']
		# submodel = subscribe_model(email = response['email'], name = response['name'], password = response['password'])
		# submodel.save()
		# return JsonResponse({'is_success' : True})


@csrf_exempt
def validate_email(request):
	if request.method == 'POST':
		check_email = subscribe_model.objects.filter(email = request.POST['email'])
		if check_email.exists() :
			return JsonResponse({'is_exist' : True})
		return JsonResponse({'is_exist' : False})

def listofsubscriber(request): 
	response = {}
	response['listofsub'] = subscribe_model.objects.all()
	html = 'listsubscriber.html'
	return render(request, html, response)

def listsubjson(request):
	sub = [obj.as_dict() for obj in subscribe_model.objects.all()]
	return JsonResponse({"results" : sub}, content_type = 'application/json')
 
def unsubscribe(request):
	if request.method == 'POST':
		email = request.POST['email']
		subscribe_model.objects.filter(email=email).delete()
		return JsonResponse({"done" : True})

