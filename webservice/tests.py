from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import webservice, listofsubscriber
from .models import subscribe_model
import unittest
import time


# Create your tests here.
class webserviceunittest(TestCase):

    def test_webservice_url_exists(self):
        response = Client().get('/webservice/')
        self.assertEqual(response.status_code, 200)

    def test_content_sub(self):
        request = HttpRequest()
        response = webservice(request)
        htmlresponse = response.content.decode('utf8')
        self.assertIn('SUBSCRIBE NOW', htmlresponse)

    def test_model_subs_can_create_data(self):
        counting_first = subscribe_model.objects.all().count()
        data = subscribe_model.objects.create(email = "email@email.com", name = "nama", password = "pw")
        counting_all_input = subscribe_model.objects.all().count()
        self.assertEqual(counting_all_input, counting_first + 1)

