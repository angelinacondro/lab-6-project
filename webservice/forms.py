from django import forms

class subscribe_form(forms.Form):
    attrsemail = {
        'id': 'email',
        'class': 'form-control',
        'placeholder': 'Enter your email here..'
    }

    attrsuser = {
        'id': 'name',
        'class': 'form-control',
        'placeholder': 'Enter your name here..'
    }

    attrspass = {
        'id': 'password',
        'class': 'form-control',
        'placeholder': 'Enter your password here..'
    }

    email = forms.EmailField(label='Email*', required=True, widget=forms.TextInput(attrs = attrsemail))
    name = forms.CharField(label = 'Name*', required=True, max_length = 30, widget=forms.TextInput(attrs = attrsuser))
    password = forms.CharField(label = 'Password*', required=True, max_length = 30, widget=forms.PasswordInput(attrs = attrspass))