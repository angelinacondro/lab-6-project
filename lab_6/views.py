from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
import urllib.request
import json
from django.http import JsonResponse
from .models import statusQ
from .forms import Status_Form

def myprofile(request):
	return render(request, "myprofile.html")
	
def labbb(request):
	html = 'labbb.html'
	response = {
		"statusform" : Status_Form(),
		"stat" : statusQ.objects.all()
	}
	form = Status_Form(request.POST or None)
	if request.method == 'POST' and form.is_valid():
		response['status_name'] = request.POST['status_name']
		stat = statusQ(status_name = response['status_name'])
		stat.save()
	return render(request, html, response)
	
def index(request):
	html = 'labbb.html'
	# response = {
	# 	"statusform" : Status_Form(),
	# 	"stat" : statusQ.objects.all()
	# }
	form = Status_Form(request.POST or None)

	return render(request, html, response)

def books(request):
	return render(request, "books.html")

def booklistjson(request):
	query = request.GET['q']
	url = "https://www.googleapis.com/books/v1/volumes?q=" + query
	response = urllib.request.urlopen(url)
	data = json.loads(response.read())
	return JsonResponse(data)
	

		
	

	