from django.conf.urls import url
from .views import labbb, myprofile, index, books, booklistjson
from webservice.views import webservice, listofsubscriber
#url for app
urlpatterns = [
	url('labbb', labbb),
	url('myprofile', myprofile),
	url('books', books),
	url('booklistjson', booklistjson, name= "booklistjson"),
	url('webservice', webservice, name='webservice'),
	url('listofsubscriber', listofsubscriber, name='listofsubs'),
	url('index', index, name='index'),
	url('', labbb)
]
