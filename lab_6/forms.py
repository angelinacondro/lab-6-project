from django import forms

class Status_Form(forms.Form):
    attrs = {
        'class': 'form-control',
        'placeholder' : 'Masukkan Status...'
    }

    status_name = forms.CharField(label='Status', required=False, max_length=300, widget=forms.TextInput(attrs=attrs))
