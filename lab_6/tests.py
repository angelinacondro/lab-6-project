from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.contrib.auth import get_user_model

from unittest import skip
from .views import labbb
from .forms import Status_Form
from .models import statusQ
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import unittest

# class lab6unittest(unittest.TestCase):
# 	def setUp(self):
# 		capabilities = {
# 			'browserName' : 'chrome',
# 			'chromeOptions' : {
# 				'useAutomationExtension' : False,
# 				'forceDevToolsScreenshot' : True,
# 				'args' : ['--headless', '--no-sandbox', 'disable-dev-shm-usage']
# 			}
# 		}
# 		self.browser = webdriver.Chrome(
# 			'./chromedriver', desired_capabilities=capabilities
# 		)

# 	def tearDown(self):
# 		self.browser.quit()

# 	def test_input_status(self):
# 		selenium = self.browser
# 		selenium.get('http://angel-status.herokuapp.com')
# 		time.sleep(5)
# 		inputan = selenium.find_element_by_id('id_status_name')
# 		# Fill the form with data
# 		inputan.send_keys('Coba Coba')
# 		inputan.submit()
# 		time.sleep(5)
# 		self.assertIn('Coba Coba', selenium.page_source)

# 	def test_layout1(self):
# 		self.browser.get('http://angel-status.herokuapp.com')
# 		self.assertIn('My Status', self.browser.title)

# 		header_text = self.browser.find_element_by_class_name('judul').text
# 		self.assertIn('Hello, apa kabar ?', header_text)
	
# 	def test_layout2(self):
# 		self.browser.get('http://angel-status.herokuapp.com')
	
# 		header = self.browser.find_element_by_class_name('flexhead').text
# 		self.assertIn('Angel', header)

# 	def test_style1(self):
# 		self.browser.get('http://angel-status.herokuapp.com')
# 		myButton = self.browser.find_element_by_tag_name('button')
# 		self.assertIn('btn btn-default', myButton.get_attribute('class'))
	
# 	def test_style2(self):
# 		self.browser.get('http://angel-status.herokuapp.com')
# 		myText = self.browser.find_element_by_id('myNavbar')
# 		self.assertIn('nav', myText.get_attribute('class'))

# 	def test_layout_myprofile(self):
# 		self.browser.get('http://angel-status.herokuapp.com/myprofile')
# 		text = self.browser.find_element_by_tag_name('h4').text
# 		self.assertIn('Angelina Condro', text)
	
# 	def test_layout_accordion(self):
# 		self.browser.get('http://angel-status.herokuapp.com/myprofile')
# 		text = self.browser.find_element_by_id('desc').text
# 		self.assertIn('Description', text)


	

if __name__ == '__main__':
	unittest.main(warnings='ignore')

class lab_6_unit_test(TestCase):
	def test_lab6_url_is_exist(self):
		response = Client().get('/lab_6/')
		self.assertEqual(response.status_code, 200)
		
	def test_content_hello(self):
		request = HttpRequest()
		response = labbb(request)
		htmlResponse = response.content.decode('utf8')
		self.assertIn('Hello, apa kabar ?', htmlResponse)
	
	def test_json_url_is_exist(self):
		response = Client().get('/booklistjson?q=quilting/')
		self.assertEqual(response.status_code, 200)

	# def test_profile_url_is_exist(self):
	# 	response = Client().get('/profile')
	# 	self.assertEqual(response.status_code, 200)

# class ModelTest(TestCase):
# 	def test_model_available(self):
# 		model1 = G(statusQ)
# 		model2 = G(statusQ)
		
# 		modelInDb = statusQ.objects.all()
# 		self.assertEquals(modelInDb.count(), 2)
		
	

# Create your tests here.
