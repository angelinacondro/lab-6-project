from django.conf.urls import url
from .views import login, logout
from lab_6.views import labbb, books
#url for app
urlpatterns = [
	url('login', login, name='login'),
	url('logout', logout, name='logout'),
	url('books', books, name='books'),
	url('', login)
]
