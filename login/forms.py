from django import forms

class Login_Form(forms.Form):
    attrs = {
        'class': 'form-control',
        'placeholder' : 'Masukkan Status...'
    }

    name = forms.CharField(label='Name', required=False, max_length=300, widget=forms.TextInput(attrs=attrs))
